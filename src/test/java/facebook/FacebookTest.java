package facebook;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import facebook.pages.FacebookFeedPage;
import facebook.pages.FacebookLoginPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import selenium.SeleniumProperties;

import static com.codeborne.selenide.Selenide.open;
import static facebook.FacebookProperties.LOGIN;
import static facebook.FacebookProperties.PASS;
import static facebook.FacebookProperties.POST_STRING;

/**
 * Created by zeburek on 05.07.2017.
 */
public class FacebookTest {
    @Before
    public void init(){
        SeleniumProperties.setDriverProperty();
        WebDriver driver = SeleniumProperties.getCustomWebDriver();
        WebDriverRunner.setWebDriver(driver);
        Configuration.browser = "chrome";
    }

    @Test
    public void userCanSearch() throws InterruptedException {
        FacebookLoginPage loginPage = open("https://www.facebook.com", FacebookLoginPage.class);
        FacebookFeedPage feedPage = loginPage.login(LOGIN,PASS);
        feedPage.enterTextToStatusField(POST_STRING);
        feedPage.submitStatus();
        feedPage.getPostedElement().shouldBe(Condition.visible);
        Assert.assertEquals(POST_STRING, feedPage.getPostedElement().getText());
    }

    @After
    public void end(){
        WebDriverRunner.closeWebDriver();
    }
}
