package facebook.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.page;

/**
 * Created by zeburek on 05.07.2017.
 */
public class FacebookLoginPage {
    private SelenideElement email;
    private SelenideElement pass;

    public FacebookFeedPage login(String email, String pass){
        this.email.val(email);
        this.pass.val(pass).pressEnter();
        return page(FacebookFeedPage.class);
    }
}
