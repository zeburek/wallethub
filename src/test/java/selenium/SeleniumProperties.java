package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zeburek on 05.07.2017.
 */
public class SeleniumProperties {
    public static File driverChrome = new File("./driver/chromedriver.exe");
    public static File driverEdge = new File("./driver/SELENIUM/MicrosoftWebDriver.exe");

    public static void setDriverProperty(){
        System.setProperty("webdriver.chrome.driver", driverChrome.getAbsolutePath());
        System.setProperty("webdriver.edge.driver", driverEdge.getAbsolutePath());
    }

    public static WebDriver getCustomWebDriver(){
        Map<String, Object> prefs = new HashMap<String, Object>();
        // Set the notification setting it will override the default setting
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);
        return new ChromeDriver(options);
    }
}
